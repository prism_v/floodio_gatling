Floodio test
=========================

To test it out, simply execute the following command:

    $mvn gatling:test

Run with parameters:

    $mvn gatling:test -DuserCount=10 -DrampUpDuration=10 -DpauseMin=1 -DpauseMax=1 -DscenarioKind=stop-after-ramp
	
Parameters:
	-DscenarioKind = skip-ramp-down | stop-after-ramp | default(optional)

