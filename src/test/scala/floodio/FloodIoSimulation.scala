package floodio

import io.gatling.core.Predef._
import floodio.requests.requests._
import floodio.properties.properties._
import floodio.scenarios.floodIoScenario._

class FloodIoSimulation extends Simulation {

  setUp(
    scenarioKind.inject(
      rampUsers(userCount) over (rampUpDuration)
      ).protocols(httpProtocol)).maxDuration(maxDuration)

}