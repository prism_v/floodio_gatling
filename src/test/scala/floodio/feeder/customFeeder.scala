package floodio.feeder

import java.io.{File, FileWriter}

import io.gatling.core.Predef.Feeder
import org.apache.commons.io.FileUtils
import io.gatling.core.Predef._

import scala.collection.mutable.ListBuffer
import scala.util.Random

object customFeeder {

  val feederFileName = "feeder.csv"
  val feederFile = new File(feederFileName)
  val randomStart = 1
  var randomEnd = 0
  var fileSize = feederFile.length()
  var itr = FileUtils.lineIterator(feederFile)
  var lineList: ListBuffer[String] = new ListBuffer[String]

  itr.next() //pass title line
  addNewLines()

  val customFeederFile = new Feeder[String] {
    override def hasNext: Boolean = true

    override def next(): Map[String, String] = {
      val currentFileSize = feederFile.length()
      if (fileSize != currentFileSize) {
        fileSize = currentFileSize
        iterateToNewLinesInFile()
        addNewLines()
      }
      val randomLineIndex = randomStart + Random.nextInt(randomEnd - randomStart)
      val age = lineList.get(randomLineIndex).toString
      Map {
        "age" -> age
      }
    }
  }

  def iterateToNewLinesInFile() = {
    itr = FileUtils.lineIterator(feederFile)
    itr.next()
    val oldRandomEnd = randomEnd
    randomEnd = randomStart
    for (i <- 1 to oldRandomEnd - 1) {
      itr.next()
      randomEnd += 1
    }
  }

  def addNewLines() = {
    while (itr.hasNext) {
      lineList += itr.next().toString
      randomEnd += 1
    }
  }

  var fw: FileWriter = _
  var number: Int = _
  val from = 18
  val to = 100

  def addRandomDataToFile() = {
    try {
      fw = new FileWriter(feederFileName, true)
      number = from + Random.nextInt(to - from)
      fw.write("\n" + number)
    }
    finally {
      fw.close()
    }
  }

}
