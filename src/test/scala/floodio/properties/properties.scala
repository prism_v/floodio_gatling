package floodio.properties

object properties {
  var userCount = 1
  var rampUpDuration = 1
  var pauseMin = 1
  var pauseMax = 1
  var scenarioKindString = "default"
  var duration = 15

  val userCountS = System.getProperty("userCount")
  val rampUpDurationS = System.getProperty("rampUpDuration")
  val pauseMinS = System.getProperty("pauseMin")
  val pauseMaxS = System.getProperty("pauseMax")
  if(userCountS != null){userCount = userCountS.toInt}
  if(rampUpDurationS != null){rampUpDuration = rampUpDurationS.toInt}
  if(pauseMinS != null){pauseMin = pauseMinS.toInt}
  if(pauseMaxS != null){pauseMax = pauseMaxS.toInt}
  if(System.getProperty("scenarioKind") != null){scenarioKindString = System.getProperty("scenarioKind")}

}
