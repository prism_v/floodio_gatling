package floodio.requests

import io.gatling.core.Predef._
import io.gatling.http.Predef.{regex, _}
import io.gatling.http.check.HttpCheck

object requests {
  val headers = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "uk-UA,uk;q=0.9,ru;q=0.8,en-US;q=0.7,en;q=0.6",
    "Connection" -> "keep-alive",
    "Upgrade-Insecure-Requests" -> "1",
    "Content-Type" -> "application/x-www-form-urlencoded")

  val params = Map(
    "utf8" -> "✓",
    "authenticity_token" -> "${token}",
    "challenger[step_id]" -> "${stepId}",
    "challenger[step_number]" -> "${stepNumber}",
    "commit" -> "${commit}"
  )

  val httpProtocol = http
    .baseURL("https://challengers.flood.io")
    .headers(headers)

  val checkToken: HttpCheck = regex("""input name="authenticity_token" type="hidden" value="(.+?)"""").find.saveAs("token")
  val checkStepId: HttpCheck = regex("""input id="challenger_step_id" name="challenger\[step_id\]" type="hidden" value="(.+?)"""").find.saveAs("stepId")
  val checkCommit: HttpCheck = regex("""name="commit" type="submit" value="(.+?)""").find.saveAs("commit")
  val checkStepNumber: HttpCheck = regex("""input id="challenger_step_number" name="challenger\[step_number\]" type="hidden" value="(.+?)"""").find.saveAs("stepNumber")

  val homePage = exec(
    http("Home page")
      .get("/")
      .headers(headers)
      .check(regex("""Welcome to our Script Challenge""").exists)
      .check(checkToken, checkStepId, checkCommit, checkStepNumber)
  )

  val step1 = exec(
    http("Step 1")
      .post("/start")
      .headers(headers)
      .formParamMap(params)
      .check(regex("""Step 2""").exists)
      .check(checkStepId, checkCommit, checkStepNumber)
  )

  val getMaxValue = exec(session => {
    val mapList = session("map").as[List[(String, String)]].toMap.mapValues(_.toInt)
    val maxValue = mapList.maxBy(_._2)
    session.set("largestOrder", maxValue._2).set("orderSelected", maxValue._1)
  })

  val step2 = exec(
    http("Step 2")
      .post("/start")
      .formParamMap(params)
      .formParam("challenger[age]", "${age}")
      .check(regex("""Step 3""").exists)
      .check(checkStepId, checkCommit, checkStepNumber)
      .check(regex("""value="(.+?)" />.+?>(\d+?)<""").ofType[(String, String)].findAll.saveAs("map"))
  )
    .exec(getMaxValue)

  val getOrderList = exec(session => {
    val orderList = session("orderList").as[List[(String, String)]]
      .map(tuple => (tuple._1.replaceAll("challenger_order_(\\d+)", "challenger[order_$1]"), tuple._2))
    session.set("orderList", orderList)
  })

  val step3 = exec(
    http("Step 3")
      .post("/start")
      .formParamMap(params)
      .formParam("challenger[largest_order]", "${largestOrder}")
      .formParam("challenger[order_selected]", "${orderSelected}")
      .check(regex("""Step 4""").exists)
      .check(checkStepId, checkCommit, checkStepNumber)
      .check(regex("""input id="(challenger_order_.+?)".+?value="(.+?)"""").ofType[(String, String)].findAll.saveAs("orderList"))
  )
    .exec(getOrderList)

  val step4 = exec(
    http("Step 4")
      .post("/start")
      .formParamMap(params)
      .formParamSeq("${orderList}")
      .check(regex("""Step 5""").exists)
      .check(checkStepId, checkCommit, checkStepNumber)
  )

  val step5 = exec(http("Step 5")
    .post("/start")
    .formParamMap(params)
    .formParam("challenger[one_time_token]", "${oneTimeToken}")
    .check(regex("""You're Done!""").exists)
  )

  val getCode = exec(http("Get code")
    .get("/code")
    .check(regex("""\{\"code\":(.+?)\}""").saveAs("oneTimeToken"))
  )

}
