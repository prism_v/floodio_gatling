package floodio.scenarios

import io.gatling.core.Predef._
import floodio.requests.requests._
import floodio.properties.properties._
import floodio.feeder.customFeeder._
import io.gatling.core.structure.ScenarioBuilder


object floodIoScenario {

  val main =
    exec(homePage)
      .pause(pauseMin, pauseMax)
      .exec(step1)
      .pause(pauseMin, pauseMax)
      .feed(customFeederFile)
      .exec(session => {
        println("AGE!!=================   " + session("age").as[String])
        addRandomDataToFile
        session
      })
      .exec(step2)
      .pause(pauseMin, pauseMax)
      .exec(step3)
      .pause(pauseMin, pauseMax)
      .exec(step4)
      .exec(getCode)
      .pause(pauseMin, pauseMax)
      .exec(step5)

  val start = System.currentTimeMillis()

  val scnDefault = scenario("BasicSimulation")
    .during(duration, exitASAP = false)(main)

  val scnStopAfterRampUp = scenario("BasicSimulation")
    .asLongAs(session => (System.currentTimeMillis - start) < rampUpDuration * 1000, exitASAP = true)(during(0, exitASAP = true)(main))

  val scnSkipRampDown = scenario("BasicSimulation")
    .asLongAs(session => (System.currentTimeMillis - start) <= (rampUpDuration + duration) * 1000, exitASAP = true)(during(duration, exitASAP = true)(main))

  def getScenarioKind(x: String): ScenarioBuilder = x match {
    case "skip-ramp-down" => scnSkipRampDown
    case "stop-after-ramp" => scnStopAfterRampUp
    case "default" => scnDefault
  }

  def getScenarioKind2(x: String): Int = x match {
    case "skip-ramp-down" => duration
    case "stop-after-ramp" => rampUpDuration
    case "default" => duration + rampUpDuration * 2
  }

  val maxDuration = getScenarioKind2(scenarioKindString)
  val scenarioKind = getScenarioKind(scenarioKindString)

  println("userCount = " + userCount)
  println("RampUpDuration = " + rampUpDuration)
  println("pauseMin = " + pauseMin)
  println("pauseMax = " + pauseMax)
  println("scenarioKind = " + scenarioKindString)
}
